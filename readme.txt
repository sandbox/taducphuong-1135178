What is Asynchronous Processing?

Asynchronous Processing is a module designed to call back ground process in Drupal.

What does it do?

The Asynchronous Processing will push the Drupal function in a queue and call the back ground process to execulate this queue.

It could be used for executing long-running tasks in the background, such as tasks involving significant network activity or encoding video, send an email etc.

Installation
1, Download & Copy this module to your module folder.
2, Copy async_process.php file to the root folder.
3, Add 2 define below in your settings.php file:
DEFINE( 'ROOT_DIR', $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . [YOUR SETUP FOLDER]);
DEFINE( 'BASE_URL2', 'http://your-website.com');
4, Enable Asynchronous Processing modules

API
function async_launch_process
param: $async_defination: the array define function will be call

Example:
/*
* Overide drupal mail
* replace to call function drupal_mail($module, $key, $to, $language, $params, $from, $send)
* */
function async_drupal_mail($module, $key, $to, $language, $params = array(), $from = NULL, $send = TRUE){
$async_defination = array('drupal_mail', $module, $key, $to, $language, $params, $from, $send);
return async_launch_process($async_defination);
}