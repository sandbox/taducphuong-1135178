<?php
// $Id: async_process.php,v 1.0 2011/04/22 15:51:33 taducphuong Exp $

/*
 * Get root folder
 * 
 * */
$currentdir = dirname(__FILE__);
$pos = strpos($currentdir, "\sites\\");
if(!$pos) $pos = strpos($currentdir, "\modules\\");
if(!$pos) $pos = strpos($currentdir, "\profiles\\");
if(!$pos) return null;

$drupal_root = substr($currentdir, 0, $pos); 

chdir($drupal_root);

define('DRUPAL_ROOT', getcwd());
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include_once './includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
global $base_url;
$base_url = variable_get('ASYNC_BASE_URL'); 
async_process_queue(true);

